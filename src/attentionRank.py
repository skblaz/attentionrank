### ranking by attention
import pandas as pd
from sklearn import preprocessing
from scipy.special import softmax as spsoftmax
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import accuracy_score

try:
    import xgboost as xgb
    import shap
except:
    shap = None
import torch
import torch.nn as nn
import numpy as np
import pickle
#from rank_module import load_and_process_generic
import logging
logging.basicConfig(format='%(asctime)s - %(message)s', datefmt='%d-%b-%y %H:%M:%S')
logging.getLogger().setLevel(logging.INFO)
import torch
import torch.nn as nn
from torch.utils.data import DataLoader, Dataset
from rank_module import *

class DatasetLoaderGeneric(Dataset):
    
    def __init__(self, file_path, labels, transform=None):
        self.data = file_path
        self.labels = labels
        
    def __len__(self):
        return len(self.data)
    
    def __getitem__(self, index):
        image = self.data[index, :]
        label = self.labels[index]            
        return image, label
    
class NeuralNet(nn.Module):
    def __init__(self, input_size, hidden_size, num_classes, dropout=0.2):
        super(NeuralNet, self).__init__()
        self.fc1 = nn.Linear(input_size, input_size)
        self.softmax = nn.Softmax(dim=1)
        self.softmax2 = nn.Softmax(dim = 0)
        self.relu = nn.SELU()
        self.dropout = nn.Dropout(dropout)
        self.fc2 = nn.Linear(input_size, hidden_size)
        self.fc3 = nn.Linear(hidden_size, num_classes)
        
    def forward(self, x):
        out = self.fc1(x)
        out = self.softmax(out)
        out = out * x
        out = self.fc2(out)
        out = self.dropout(out)
        out = self.fc3(out)
        return out
    
    def get_attention(self,x):
        out = self.fc1(x)
        out = self.softmax(out)
        return out.cpu()
    
    def get_hadamand_layer(self):
        return self.fc1.weight.data
    
    def get_softmax_hadamand_layer(self):
        return self.softmax2(torch.diag(self.fc1.weight.data)).cpu()


def simple_ffNN(train_features,test_features,train_tars,test_tars,architecture=[512,20,100,0.001,0.2]):

    train_features = train_features.values
    try:
        test_features = test_features.values
        test_dataset = DatasetLoaderGeneric(test_features,test_tars)
    except Exception as es:
#        logging.info(es)
        logging.info("explain mode only")
    
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    hidden_size = architecture[0]
    num_epochs = architecture[1]
    batch_size = architecture[2]
    num_tar_class = len(np.unique(train_tars))
    learning_rate = architecture[3]
    dropout = architecture[4]
    train_dataset = DatasetLoaderGeneric(train_features,train_tars)
    train_loader = torch.utils.data.DataLoader(dataset=train_dataset, 
                                               batch_size=batch_size, 
                                               shuffle=True)

    model = NeuralNet(train_features.shape[1], hidden_size, num_tar_class, dropout).to(device)
    criterion = nn.CrossEntropyLoss()
    optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)
    pytorch_total_params = sum(p.numel() for p in model.parameters())
    
#    logging.info("Total number of parameters: {}".format(pytorch_total_params))
    # Train the model
    
    total_step = len(train_loader)
    for epoch in range(num_epochs):
        for i, (features, labels) in enumerate(train_loader):
            try:
                features = features.float().to(device)
                labels = labels.to(device)

                # Forward pass
                outputs = model.forward(features)
                loss = criterion(outputs, labels)

                # Backward and optimize
                optimizer.zero_grad()
                loss.backward()
                optimizer.step()
            except Exception as es:
                print(es)
#                print(features,labels)

        # if epoch % 5 == 0:
        #     logging.info ('Epoch {}, Loss: {:.4f}'.format(epoch,loss.item()))

    if test_features is not None:
        test_loader = torch.utils.data.DataLoader(dataset=test_dataset, 
                                                  batch_size=batch_size, 
                                                  shuffle=False)
        with torch.no_grad():
            correct = 0
            total = 0
            for features, labels in train_loader:                
                #print(features, labels)
                features = features.float().to(device)
                labels = labels.to(device)
                outputs = model.forward(features)
                attention = model.get_attention(features)

                _, predicted = torch.max(outputs.data, 1)
                total += labels.size(0)
                correct += (predicted == labels).sum().item()
            return (100 * correct / total)
    else:
        attentions = []
        clean_attentions = []
        softmax_attentions = []
        logging.info("Starting to explain..")
        with torch.no_grad():
            for features, labels in train_loader:
                features = features.float().to(device)
                labels = labels.to(device).cpu().numpy()
                outputs = model.forward(features)
                _, predicted = torch.max(outputs.data, 1)
                predicted = predicted.cpu().numpy()
                aggs = []
                for enx, el in enumerate(labels):
                    if predicted[enx] == el:
                        aggs.append(features[enx].cpu().numpy())

                aggs = torch.from_numpy(np.matrix(aggs)).to(device)
                clean_att = model.get_attention(aggs)
                clean_attentions.append(clean_att)
                
                attention = model.get_attention(features)                
                attentions.append(attention)

        whole_layer = model.get_softmax_hadamand_layer()
#        self_attention = whole_layer.diag().cpu().numpy()
        return attentions, clean_attentions, whole_layer #self_attention

    #torch.save(model.state_dict(), 'model.ckpt')
    
def load_and_process_anuran(dname):
    data = pd.read_csv(dname,sep=",")
    train_data = data.iloc[:,:-4]
    target_first = data.iloc[:,-4]
    target_second = data.iloc[:,-3]
    return (train_data,target_first,target_second)

def simple_xgb(train_features,test_features=None,train_tars=None,test_tars=None,grid=[100,20,0.02,10,20,10],explainer=False):

    param = {}
    param['estimators'] = grid[0]
    param['max_depth'] = grid[1]
    param['learning_rate'] = grid[2]
    param['objective'] = 'multi:softprob'
    param['min_child_weight'] = grid[3]
    param['gamma'] = grid[4]
    param['calsample_bytree'] = grid[5]
    param['num_class'] = np.max(train_tars)+1
    clf = xgb.train(param, xgb.DMatrix(train_features, label=train_tars))
    if explainer:
        explainer = shap.TreeExplainer(clf)
        logging.info(train_features.shape)
        shap_values = explainer.shap_values(train_features)[1,:]
        shap_values = shap_values[:,0:train_features.shape[1]]
        logging.info(shap_values.shape)
#        shap.summary_plot(shap_values, train_features)
#        plt.show()
        return shap_values
    else:
        assert set(test_features.columns) == set(train_features.columns)
        test_features = xgb.DMatrix(test_features)
        preds = clf.predict(test_features)
        preds = np.argmax(preds, axis=1)
        return accuracy_score(preds,test_tars)

if __name__ == "__main__":

    import argparse
    import glob
    parser = argparse.ArgumentParser()
    parser.add_argument("--dataset",default="../data/stratified/fold1")
    parser.add_argument("--method",default="attention")
    args = parser.parse_args()
    unique_datasets = set(x.split("_")[0] for x in glob.glob(args.dataset+"/*"))
    for dataset in unique_datasets:

        overall_acc = []

        learning_rates = [0.001,0.01,0.00001]#[0.001,0.1,0.0001, 0.00001]
        batch_size = [32]
        dropouts = [0.2,0,0.5]#[0.01,0.05,0.1,0.2,0.3,0.5,0.8]
        num_epoch = [5,20]#[5,10,15,20,100, 200]
        hidden_size = [128,64]#[128,256,512,1024]
        grid_nn = []
        
        for l in learning_rates:
            for b in batch_size:
                for e in num_epoch:
                    for h in hidden_size:
                        for d in dropouts:
                            grid_nn.append((h,e,b,l,d))

        xgb_grid = []
        # grid_xgb = {"learning_rate"    : [0.05] ,
        #             "max_depth"        : [ 3],
        #             "min_child_weight" : [ 1],
        #             "gamma"            : [ 0.0],
        #             "estimators"            : [10],
        #             "colsample_bytree" : [ 0.3 ] }

        grid_xgb = {"learning_rate"    : [0.05, 0.30, 0.1, 0.9 ] ,
                    "max_depth"        : [ 3, 10, 15, 30],
                    "min_child_weight" : [ 1,  5 , 3],
                    "gamma"            : [ 0.0, 0.1, 0.2 , 0.3, 0.4 , 1],
                    "estimators"            : [100, 300],
        "colsample_bytree" : [ 0.3, 0.7 ] }

        for e in grid_xgb['estimators']:
            for d in grid_xgb['max_depth']:
                for lr in grid_xgb['learning_rate']:
                    for mcw in grid_xgb['min_child_weight']:
                        for g in grid_xgb['gamma']:
                            for cs in grid_xgb['colsample_bytree']:
                                xgb_grid.append([e,d,lr,mcw,g,cs])

        grid_search = {"attention":grid_nn,"xgb":xgb_grid}
        top_acc = 0
        top_setting = None
        logging.info("Ready to explore grid of size: attention: {}, xgb: {}".format(len(grid_nn),len(xgb_grid)))
        cntx = 0
        for parameter_setting in grid_search[args.method]:
            logging.info("Testing {} setting.".format(parameter_setting))
            cntx+=1
            for k in range(9): ## go through all folds here.
                x = k + 1
                logging.info("Currently at fold {}".format(x))
                fold_id = x+1
                dname_train = dataset.replace("fold1","fold{}".format(str(fold_id)))+"_train.csv"
                dname_test = dataset.replace("fold1","fold{}".format(str(fold_id)))+"_test.csv"
                train_instances_real, target_first_train = load_and_process_generic(dname_train)
                test_instances_real, target_first_test = load_and_process_generic(dname_test)
                if train_instances_real is None:
                    continue
                enc0 = preprocessing.LabelEncoder()
                enc1 = preprocessing.OneHotEncoder()
                train_tars = enc0.fit_transform(target_first_train)
                test_tars = enc0.transform(target_first_test)
                train_instances = train_instances_real
                test_instances = test_instances_real
                logging.info(train_instances.shape)
                assert set(train_instances.columns) == set(test_instances.columns)

                if args.method == "attention":
                    acc = simple_ffNN(train_instances,test_instances,train_tars,test_tars,parameter_setting)
                if args.method == "xgb":
                    acc = simple_xgb(train_instances,test_instances,train_tars,test_tars,parameter_setting)

                overall_acc.append(acc)
            acc = np.mean(overall_acc)
            if top_acc < acc:
                logging.info("Improved accuracy to {}%".format(acc))
                top_acc = acc
                top_setting = parameter_setting

            if top_acc >= 0.95:
                break

        ## whole dataset ranks
        train_instances = pd.concat([train_instances,test_instances])
        targets = np.concatenate([train_tars,test_tars])
        logging.info("finshed folding")
        
        ## compute overall attention
        if args.method == "attention":

            ## attentions with correct weights

            ## all attentions
            attentions, clean_attentions, self_attention = simple_ffNN(train_instances,None,targets, None,top_setting)
            all_at = torch.cat(attentions)
            all_at = torch.sum(all_at,0).cpu()
            feature_importances = all_at.numpy()
            
            cnames = train_instances_real.columns
            dataset = dataset.split("/")[-1]+".pickle"

            ## core attention
            out_result = {"importances":feature_importances,"names":cnames,"dataset":dataset,"top_acc":top_acc,"top_param":top_setting,"method":args.method}
            logging.info("Successfully stored results for attention: {}, {}".format(args.method, dataset))
            with open('../results/'+args.method+"_"+dataset, 'wb') as handle:
                pickle.dump(out_result, handle, protocol=pickle.HIGHEST_PROTOCOL)

            all_at = torch.cat(clean_attentions)
            all_at = torch.sum(all_at,0).cpu()
            feature_importances = all_at.numpy()
                
            ## clean attention
            out_result = {"importances":feature_importances,"names":cnames,"dataset":dataset,"top_acc":top_acc,"top_param":top_setting,"method":args.method+"Clean"}
            logging.info("Successfully stored results for clean attention: {}, {}".format(args.method, dataset))
            with open('../results/'+args.method+"Clean_"+dataset, 'wb') as handle:
                pickle.dump(out_result, handle, protocol=pickle.HIGHEST_PROTOCOL)

            ## weights and softmax weights            
            out_result = {"importances":self_attention,"names":cnames,"dataset":dataset,"top_acc":top_acc,"top_param":top_setting,"method":args.method+"Layer"}
            logging.info("Successfully stored results for layer attention: {}, {}".format(args.method, dataset))
            with open('../results/'+args.method+"Layer_"+dataset, 'wb') as handle:
                pickle.dump(out_result, handle, protocol=pickle.HIGHEST_PROTOCOL)
                
        elif args.method == "xgb":
            pass
        
            # shap_values = simple_xgb(train_instances,None,targets, None,top_setting,explainer=True)
            # feature_importances = np.mean(shap_values,axis=0)
            # assert len(feature_importances) == len(train_instances.columns)

            # cnames = train_instances_real.columns
            # dataset = dataset.split("/")[-1]
            # out_result = {"importances":feature_importances,"names":cnames,"dataset":dataset,"top_acc":top_acc,"top_param":top_setting,"method":args.method}    
            # logging.info("Successfully stored results for: {}, {}".format(args.method, dataset))
            # with open('../results/'+args.method+"_"+dataset, 'wb') as handle:
            #     pickle.dump(out_result, handle, protocol=pickle.HIGHEST_PROTOCOL)
        else:
            logging.info("No method {}".format(args.method))
