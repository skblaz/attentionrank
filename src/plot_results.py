### simple plot and analysis of obtained results

import os
import glob
import numpy as np
import tqdm
import pickle
import matplotlib.pyplot as plt
from matplotlib import rc, font_manager
from scipy.integrate import simps
import itertools
import seaborn as sns
import pandas as pd
from scipy.spatial import distance
from fuji import *

font_size = 20
font_properties = {'family': 'serif', 'serif': ['Computer Modern Roman'],
                   'weight': 'normal', 'size': font_size}

font_manager.FontProperties(family='Computer Modern Roman', style='normal',
                            size=font_size, weight='normal', stretch='normal')
rc('text', usetex=True)
rc('font', **font_properties)
sns.set_style("whitegrid")
def plot_feature_rankings(importances,names,algo):
    tbx = pd.DataFrame({"importances":importances,"names":names})
    tbx = tbx.sort_values(['importances']).reset_index(drop=True)
    sns.barplot(tbx.names,tbx.importances,palette='Blues_d',label=algo)
    plt.xticks(rotation='vertical')

def simple_comparison_plot(dataset_name,results_folder):
    feature_obj = {}
    for result_file in glob.glob(results_folder):
        # do this for each dataset
        if dataset_name in result_file:
            with open(result_file, 'rb') as file:
                results = pickle.load(file)
                feature_names = results['names']
                importances = results['importances']
#                importances = (importances - np.min(importances))/(np.max(importances)-np.min(importances))
#                importances = importances / (len(feature_names)**2)
                method = os.path.basename(result_file).split("_")[0]                
                if len(importances) == len(feature_names):
                    dataset = results['dataset']
                    try:
                        top_acc = results['top_acc']                    
                        top_param = results['top_param']
                    except:
                        top_acc = []
                        top_param = []
                    feature_obj[method] = {"feature_names":feature_names,"importances":importances,"top_param":top_param,"top_acc":top_acc}

                else:
                    print(method)

    return feature_obj


def get_dist(feature_object,dname):
    fnames = None
    outs = {}
    for k,v in feature_object.items():
        outs[k] = (v['importances'],v['feature_names'])
    ents = None
    distance = fuzzy_jaccard(outs['ReliefF'],outs['ReliefF'])
    dists = []
    for com in tqdm.tqdm(itertools.combinations(outs.keys(),2)):
        al1 = com[0]
        al2 = com[1]
        distance = fuzzy_jaccard(outs[com[0]],outs[com[1]])
        dists.append((al1,al2,distance))
    return dists

def plot_overall_variability(dfx):

    sns.barplot(dfx.dataset,dfx.distance)
    plt.show()
    
def plot_overall_pairs(dfx):
    sns.barplot(dfx.dataset,dfx.distance,hue=dfx.algo)
    plt.show()
    
if __name__ == "__main__":

    
    results_folder = "../results/*"
    eval_file = "../final_results/ranking_results.txt"
    rows = []

    # for flx in glob.glob(rfold_folder):
    #     if "MI_" in flx:
    #         with open(flx, 'rb') as file:
    #             fx = pickle.load(file)            
    
    with open(eval_file) as evf:
        for line in evf:
            line = line.strip().split()
            if len(line) > 0:
                if line[0] == "RESULT_LINE":
                    try:
                        row = {"num_top":int(line[1]),"classifier":line[2],"acc":float(line[3]),"F1":float(line[4]),"dataset":line[5],"ranking":line[6],"fold":int(line[7])}
                        rows.append(row)
                    except Exception as es:
                        print(es)
                        print(line)

    pdx = pd.DataFrame(rows)
    pdx = pdx[pdx['classifier'] == "LR"]
    pdx = pdx.groupby(['num_top','ranking','dataset']).mean().reset_index()
    pdx = pdx[pdx['ranking'] != "xgb"]

    uniq_dx = pdx['dataset'].unique()
    for enx, fx in enumerate(uniq_dx):
        subs = pdx[pdx['dataset'] == fx]        
        une = pdx.ranking.unique()
        markerss = ('o', 'v', '^', '<', '>', '8', 's', 'p', '*', 'h', 'H', 'D', 'd', 'P', 'X')
        g = sns.lineplot(subs.num_top,subs.F1,hue=subs.ranking)
        plt.xlabel(r"Top features",labelpad=-5)
        plt.ylabel(r"Performance ($F1$)")
        plt.title(r"Dataset: {}".format(fx))
        if "dlbcl" in fx:
            print(fx)
            plt.legend(prop={'size': 16})
        else:
            g.get_legend().remove()
        plt.savefig(r"../result_figure/overall_visualization_{}.png".format(fx), dpi=300)
        plt.clf()

    unique_datasets = []
    for result in glob.glob(results_folder):
        print(result)
        res = result.split("_")[1]
        unique_datasets.append(res)

    dataset_dists = []
    dfx = {}
    performances = []
    integrated_overall = []
    for enx, el in enumerate(list(set(unique_datasets))):
        print(el)
        feature_object = simple_comparison_plot(el,results_folder)
        for k,v in feature_object.items():
            performances.append({"model":k,"top_param":v['top_param'],"top_acc":v['top_acc'],"dataset":el})

        try:
            distances = get_dist(feature_object, el)
            dfx[el.replace(".gpickle","")] = distances
            for dist in distances:
                if "attention" in dist[0] or "attention" in dist[1]:
                    
                    plt.plot(dist[2],label=dist[0].replace("attention","att")+" vs. "+dist[1].replace("attention","att"))
                integrated = simps(dist[2], range(len(dist[2])))
                integrated = integrated / len(dist[2])
                integrated_overall.append({"method1":dist[0],"method2":dist[1],"integral":integrated})

            if "dlbcl" in el:
                plt.legend(prop={'size': 13}, ncol = 2)
#                legend(loc="upper left", bbox_to_anchor=(1,1))
#            plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05), shadow=True, ncol=4,prop={'size': 12})
#            plt.tight_layout()
            plt.xlabel(r"Features",labelpad=-5)
            plt.ylabel(r"Fuji score")
            plt.title(r"Dataset: {}".format(el.replace(".pickle","")))
            fnx = el.replace(".pickle",".png")
            plt.tight_layout()
            if "dlbcl" in el:
                plt.show()
            else:
                plt.savefig(r"../result_figure/fuji_"+fnx)
            plt.clf()
            
        except Exception as es:
            print("*"*10,es)

    dfx_int = pd.DataFrame(integrated_overall)

    # dfx_int.method1 = dfx_int.method1.replace({"forest" : "RandomForest",
    #                                            "relief" : "ReliefF",
    #                                            "MI" : "MutualInformation",
    #                                            "attentionClean":"attentionPositive",
    #                                            "attentionLayer":"attentionGlobal"})
    
    # dfx_int.method2 = dfx_int.method2.replace({"forest" : "RandomForest",
    #                                            "relief" : "ReliefF",
    #                                            "MI" : "MutualInformation",
    #                                            "attentionClean":"attentionPositive",
    #                                            "attentionLayer":"attentionGlobal"})
                                         
    dfx_int = dfx_int.groupby(['method1','method2']).mean().reset_index()
    hmx = dfx_int.pivot(index='method1', columns='method2', values='integral')
    matrix = np.triu(hmx)
    ax = sns.heatmap(hmx, cmap = "BrBG", linewidths = .5, linecolor = "white", square = True, annot = True, mask = matrix, annot_kws={"size": 20})
    plt.xlabel("", fontsize = 20)
    plt.ylabel("", fontsize = 20)
    plt.show()
    plt.clf()
    
    performance_frame = pd.DataFrame(performances)
    performance_frame.to_latex("../results_tables/main.tex")

    knn_file = "../final_results/knn_results.txt"
    knx = pd.read_csv(knn_file)
    knx.columns = ["index","sth","Accuracy","F1","k","method"]
    sns.lineplot(knx.k,knx.Accuracy,hue=knx.method)
    plt.legend(prop={'size': 9})
    plt.xlabel(r"Num. nearest neighbors (k)",labelpad=-5)
    plt.savefig(r"../result_figure/knn_visualization_{}.png".format("KNN"),dpi=300)
