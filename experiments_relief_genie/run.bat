java -Xmx20G -jar clus.jar -forest aps-failure_forest.s >> output.log 2>>&1
java -Xmx20G -jar clus.jar -relief aps-failure_relief.s >> output.log 2>>&1
java -Xmx20G -jar clus.jar -forest genes_forest.s >> output.log 2>>&1
java -Xmx20G -jar clus.jar -relief genes_relief.s >> output.log 2>>&1
java -Xmx20G -jar clus.jar -forest p53_forest.s >> output.log 2>>&1
java -Xmx20G -jar clus.jar -relief p53_relief.s >> output.log 2>>&1
java -Xmx20G -jar clus.jar -forest pd-speech-features_forest.s >> output.log 2>>&1
java -Xmx20G -jar clus.jar -relief pd-speech-features_relief.s >> output.log 2>>&1
