[General]
ResourceInfoLoaded = Yes
Verbose = 1

[Data]
File = ../data/arff_datasets/aps-failure.arff

[Attributes]
Target = 171



[Ensemble]
Iterations = 100
NumberOfThreads = 1
EnsembleMethod = RForest
FeatureRanking = Genie3
SelectRandomSubspaces = SQRT
Optimize = No
PrintAllModels = No
PrintAllModelFiles = No
PrintAllModelInfo = No
PrintPaths = No
OOBestimate = No

[Relief]
Neighbours = 15

[Output]
WritePredictions = [Test]

