import os
import re


def arff_to_csv(arff, out_dir, class_index=None):
    def process_feature(line):
        pattern = "@attribute +([^ ]+) +(.+)"
        m = re.search(pattern, line)
        name = m.group(1)
        values = m.group(2)
        if feature_ind != class_index and values.startswith("{") and values.endswith("}"):
            # not class but nominal
            values = [v.strip() for v in values[1:-1].split(",")]
            if len(values) == 2:
                values = values[:1]
            return name, values
        else:
            # leave it as it is
            return name, [values]

    def one_hot_vector(v, values):
        if v == "?":
            return ["?" for _ in range(len(values))]
        else:
            return [str(int(v == u)) for u in values]

    def compute_class_index():
        i_class = None
        attributes = 0
        with open(arff) as f:
            for l in f:
                if l.lower().startswith("@data"):
                    break
                elif l.lower().startswith("@attribute"):
                    attributes += 1
                    if l.lower().startswith("@attribute class"):
                        i_class = attributes
        return i_class - 1  # for preventing None ...
    os.makedirs(out_dir, exist_ok=True)
    list_of_binary = []
    n_features = 0
    feature_ind = 0
    indices_old = {}
    header = []
    if class_index is None:
        class_index = compute_class_index()
    with open(arff) as f:
        for x in f:
            a = x.strip()
            if a.lower().startswith("@attribute"):
                a_name, a_values = process_feature(a)
                needs_processing = a_values[0] != "numeric" and feature_ind != class_index
                if needs_processing:
                    indices_old[feature_ind] = [t for t in a_values]
                    list_of_binary.append([])
                feature_ind += 1
                for value in a_values:
                    if needs_processing:
                        list_of_binary[-1].append(n_features)
                        header.append("{}_{}".format(a_name, value))
                    else:
                        header.append("{}".format(a_name))
                    n_features += 1
            elif a.lower().startswith("@data"):
                break

        a_name = os.path.join(out_dir, os.path.basename(arff))
        a_name = a_name[:a_name.rfind('.')] + ".csv"
        with open(a_name, "w", newline='') as g:
            print(','.join(header), file=g)
            for x in f:
                a = x.strip()
                if not a:
                    continue
                else:
                    row_values = [t.strip() for t in a.split(",")]
                    new_values = ["" for _ in range(n_features)]
                    where_to = 0
                    for i in range(len(row_values)):
                        if i in indices_old:
                            vector = one_hot_vector(row_values[i], indices_old[i])
                            for t in vector:
                                new_values[where_to] = t
                                where_to += 1
                        else:
                            new_values[where_to] = row_values[i]
                            where_to += 1
                print(",".join(new_values), file=g)
    return a_name, list_of_binary


data_dir = "C:/Users/matej/git/e8datasets/STC/"
multi_class_dir = data_dir + "Multiclass/"
two_class_dir = data_dir + "Binary/"
multi_data = [multi_class_dir + a + ".arff" for a in ["biodeg-p2-discrete", "optdigits"]]
binary_data = [two_class_dir + a + ".arff" for a in ["madelon", "dlbcl", "chess"]]
data = multi_data + binary_data
if 0:
    for d in data:
        arff_to_csv(d, "../data/")
