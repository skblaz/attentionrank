### ranking by attention
import pandas as pd
from sklearn import preprocessing
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import accuracy_score
import xgboost as xgb
import shap
import torch
import torch.nn as nn
import numpy as np
from arcs import *
import matplotlib.pyplot as plt
import seaborn as sns
import pickle
from attentionRank import load_and_process_generic


def simple_ffNN(train_features, test_features, train_tars, test_tars, architecture=[512, 20, 100, 0.001]):
    train_features = train_features.values
    try:
        test_features = test_features.values
    except:
        pass
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    hidden_size = architecture[0]
    num_epochs = architecture[1]
    batch_size = architecture[2]
    num_tar_class = len(np.unique(train_tars))
    learning_rate = architecture[3]

    train_dataset = DatasetFrog(train_features, train_tars)
    test_dataset = DatasetFrog(test_features, test_tars)
    train_loader = torch.utils.data.DataLoader(dataset=train_dataset,
                                               batch_size=batch_size,
                                               shuffle=True)

    model = NeuralNet(train_features.shape[1], hidden_size, num_tar_class).to(device)
    criterion = nn.CrossEntropyLoss()
    optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)
    pytorch_total_params = sum(p.numel() for p in model.parameters())

    #    print("Total number of parameters: {}".format(pytorch_total_params))
    # Train the model

    total_step = len(train_loader)
    for epoch in range(num_epochs):
        for i, (features, labels) in enumerate(train_loader):
            features = features.float().to(device)
            labels = labels.to(device)

            # Forward pass
            outputs = model.forward(features)
            loss = criterion(outputs, labels)

            # Backward and optimize
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            # if epoch % 5 == 0:
            #     print ('Epoch {}, Loss: {:.4f}'.format(epoch,loss.item()))

    if test_features is not None:
        test_loader = torch.utils.data.DataLoader(dataset=test_dataset,
                                                  batch_size=batch_size,
                                                  shuffle=False)
        with torch.no_grad():
            correct = 0
            total = 0
            for features, labels in train_loader:
                features = features.float().to(device)
                labels = labels.to(device)
                outputs = model.forward(features)
                attention = model.get_attention(features)

                _, predicted = torch.max(outputs.data, 1)
                total += labels.size(0)
                correct += (predicted == labels).sum().item()
            return (100 * correct / total)
    else:
        attentions = []
        with torch.no_grad():
            for features, labels in train_loader:
                features = features.float().to(device)
                labels = labels.to(device)
                outputs = model.forward(features)
                attention = model.get_attention(features)
                attentions.append(attention)
        return attentions

        # torch.save(model.state_dict(), 'model.ckpt')


# def load_and_process_generic(dname):
#     data = pd.read_csv(dname, sep=",")
#     if "class" in data.columns:
#         cols = [x for x in data.columns if x != "class"]
#         train_data = data[cols]
#         target = data[["class"]]
#         return train_data, target
#     else:
#         return (None, None)


def simple_xgb(train_features, test_features=None, train_tars=None, test_tars=None, grid=[100, 20, 0.02, 10, 20, 10],
               explainer=False):
    param = {}
    param['estimators'] = grid[0]
    param['max_depth'] = grid[1]
    param['learning_rate'] = grid[2]
    param['objective'] = 'multi:softprob'
    param['min_child_weight'] = grid[3]
    param['gamma'] = grid[4]
    param['calsample_bytree'] = grid[5]
    param['verbose_eval'] = 50
    param['num_class'] = np.max(train_tars) + 1
    clf = xgb.train(param, xgb.DMatrix(train_features, label=train_tars))
    if explainer:
        explainer = shap.TreeExplainer(clf)
        print(train_features.shape)
        shap_values = explainer.shap_values(train_features)
        shap_values = shap_values[:, 0:train_features.shape[1]]
        print(shap_values.shape)
        #        shap.summary_plot(shap_values, train_features)
        #        plt.show()
        return shap_values
    else:
        assert set(test_features.columns) == set(train_features.columns)
        test_features = xgb.DMatrix(test_features)
        preds = clf.predict(test_features)
        preds = np.argmax(preds, axis=1)
        return accuracy_score(preds, test_tars)


def shap_rankings(dataset):
    train_instances_real, target_first = load_and_process_generic(dataset)
    enc0 = preprocessing.LabelEncoder()
    enc1 = preprocessing.OneHotEncoder()
    target_first_encoded = enc0.fit_transform(target_first)
    skf = StratifiedKFold(n_splits=3)

    train_instances = train_instances_real
    overall_acc = []

    xgb_grid = []
    grid_xgb = {"learning_rate": [0.05],
                "max_depth": [10],
                "min_child_weight": [5],
                "gamma": [0.2],
                "estimators": [100],
                "colsample_bytree": [0.3]}
    # grid_xgb = {"learning_rate"    : [0.05, 0.30 ] ,
    #             "max_depth"        : [ 3, 10, 15],
    #             "min_child_weight" : [ 1,  5 ],
    #             "gamma"            : [ 0.0, 0.1, 0.2 , 0.3, 0.4 ],
    #             "estimators"            : [100],
    # "colsample_bytree" : [ 0.3, 0.7 ] }

    for e in grid_xgb['estimators']:
        for d in grid_xgb['max_depth']:
            for lr in grid_xgb['learning_rate']:
                for mcw in grid_xgb['min_child_weight']:
                    for g in grid_xgb['gamma']:
                        for cs in grid_xgb['colsample_bytree']:
                            xgb_grid.append([e, d, lr, mcw, g, cs])

    top_acc = 0
    top_setting = None
    for parameter_setting in xgb_grid:
        print("Testing {} setting.".format(parameter_setting))
        for train_index, test_index in skf.split(train_instances, target_first_encoded):
            train_features = train_instances.iloc[train_index, :]
            test_features = train_instances.iloc[test_index, :]
            assert set(train_features.columns) == set(test_features.columns)
            train_tars = target_first_encoded[train_index]
            test_tars = target_first_encoded[test_index]
            acc = simple_xgb(train_features, test_features, train_tars, test_tars, parameter_setting)
            overall_acc.append(acc)
        acc = np.mean(overall_acc)
        if top_acc < acc:
            print("Improved accuracy to {}%".format(acc))
            top_acc = acc
            top_setting = parameter_setting

    shap_values = simple_xgb(train_instances, None, target_first_encoded, None, top_setting, explainer=True)
    feature_importances = np.mean(shap_values, axis=0)
    assert len(feature_importances) == len(train_instances.columns)
    cnames = train_instances_real.columns
    out_result = {"importances": feature_importances, "names": cnames, "dataset": dataset, "top_acc": top_acc,
                  "top_param": top_setting, "method": "xgb"}
    return out_result


def attention_rankings(dataset):
    train_instances_real, target_first = load_and_process_generic(dataset)
    enc0 = preprocessing.LabelEncoder()
    enc1 = preprocessing.OneHotEncoder()
    target_first_encoded = enc0.fit_transform(target_first)
    skf = StratifiedKFold(n_splits=3)

    train_instances = train_instances_real
    overall_acc = []

    # learning_rates = [0.001,0.1,0.0001]
    # batch_size = [10,20,100]
    # num_epoch = [5,10,15,20,100]
    # hidden_size = [128,256,512,1024]

    learning_rates = [0.001]
    batch_size = [10]
    num_epoch = [20]
    hidden_size = [32]
    grid_nn = []
    for l in learning_rates:
        for b in batch_size:
            for e in num_epoch:
                for h in hidden_size:
                    grid_nn.append((h, e, b, l))

    top_acc = 0
    top_setting = None
    ## do a split on the train set
    for parameter_setting in grid_nn:
        print("Testing {} setting.".format(parameter_setting))
        for train_index, test_index in skf.split(train_instances, target_first_encoded):
            train_features = train_instances.iloc[train_index, :]
            test_features = train_instances.iloc[test_index, :]
            assert set(train_features.columns) == set(test_features.columns)
            train_tars = target_first_encoded[train_index]
            test_tars = target_first_encoded[test_index]
            acc = simple_ffNN(train_features, test_features, train_tars, test_tars, parameter_setting)

            overall_acc.append(acc)
        acc = np.mean(overall_acc)
        if top_acc < acc:
            print("Improved accuracy to {}%".format(acc))
            top_acc = acc
            top_setting = parameter_setting

    attentions = simple_ffNN(train_instances, None, target_first_encoded, None, top_setting)
    all_at = torch.cat(attentions)
    all_at = torch.sum(all_at, 0).cpu()
    feature_importances = all_at.numpy()
    cnames = train_instances_real.columns
    out_result = {"importances": feature_importances, "names": cnames, "dataset": dataset, "top_acc": top_acc,
                  "top_param": top_setting, "method": "attention"}
    return out_result


if __name__ == "__main__":

    import argparse
    import glob

    parser = argparse.ArgumentParser()
    parser.add_argument("--dataset", default="../data")
    parser.add_argument("--method", default="attention")
    args = parser.parse_args()

    for dataset in glob.glob(args.dataset + "/*"):
        dataset_name = dataset.split("/")[-1].replace(".csv", "") + ".pickle"
        train_instances_real, target_first = load_and_process_generic(dataset)
        if train_instances_real is None:
            continue
        enc0 = preprocessing.LabelEncoder()
        enc1 = preprocessing.OneHotEncoder()
        target_first_encoded = enc0.fit_transform(target_first)
        skf = StratifiedKFold(n_splits=10)

        train_instances = train_instances_real
        overall_acc = []

        learning_rates = [0.001, 0.1, 0.0001]
        batch_size = [10, 20, 100]
        num_epoch = [5, 10, 15, 20, 100]
        hidden_size = [128, 256, 512, 1024]
        grid_nn = []
        for l in learning_rates:
            for b in batch_size:
                for e in num_epoch:
                    for h in hidden_size:
                        grid_nn.append((h, e, b, l))

        xgb_grid = []
        # grid_xgb = {"learning_rate"    : [0.05] ,
        #             "max_depth"        : [ 3],
        #             "min_child_weight" : [ 1],
        #             "gamma"            : [ 0.0],
        #             "estimators"            : [10],
        #             "colsample_bytree" : [ 0.3 ] }

        grid_xgb = {"learning_rate": [0.05, 0.30],
                    "max_depth": [3, 10, 15],
                    "min_child_weight": [1, 5],
                    "gamma": [0.0, 0.1, 0.2, 0.3, 0.4],
                    "estimators": [100],
                    "colsample_bytree": [0.3, 0.7]}

        for e in grid_xgb['estimators']:
            for d in grid_xgb['max_depth']:
                for lr in grid_xgb['learning_rate']:
                    for mcw in grid_xgb['min_child_weight']:
                        for g in grid_xgb['gamma']:
                            for cs in grid_xgb['colsample_bytree']:
                                xgb_grid.append([e, d, lr, mcw, g, cs])

        grid_search = {"attention": grid_nn, "xgb": xgb_grid}
        top_acc = 0
        top_setting = None
        for parameter_setting in grid_search[args.method]:
            print("Testing {} setting.".format(parameter_setting))
            for train_index, test_index in skf.split(train_instances, target_first_encoded):
                train_features = train_instances.iloc[train_index, :]
                test_features = train_instances.iloc[test_index, :]
                assert set(train_features.columns) == set(test_features.columns)
                train_tars = target_first_encoded[train_index]
                test_tars = target_first_encoded[test_index]

                if args.method == "attention":
                    acc = simple_ffNN(train_features, test_features, train_tars, test_tars, parameter_setting)
                if args.method == "xgb":
                    acc = simple_xgb(train_features, test_features, train_tars, test_tars, parameter_setting)

                overall_acc.append(acc)
            acc = np.mean(overall_acc)
            if top_acc < acc:
                print("Improved accuracy to {}%".format(acc))
                top_acc = acc
                top_setting = parameter_setting

        ## compute overall attention
        if args.method == "attention":
            attentions = simple_ffNN(train_instances, None, target_first_encoded, None, top_setting)
            all_at = torch.cat(attentions)
            all_at = torch.sum(all_at, 0).cpu()
            feature_importances = all_at.numpy()

        if args.method == "xgb":
            shap_values = simple_xgb(train_instances, None, target_first_encoded, None, parameter_setting,
                                     explainer=True)
            feature_importances = np.mean(shap_values, axis=0)
            assert len(feature_importances) == len(train_instances.columns)
        else:
            print("No method")

        cnames = train_instances_real.columns
        out_result = {"importances": feature_importances, "names": cnames, "dataset": dataset, "top_acc": top_acc,
                      "top_param": top_setting, "method": args.method}
        with open('../results/' + args.method + "_" + dataset_name, 'wb') as handle:
            pickle.dump(out_result, handle, protocol=pickle.HIGHEST_PROTOCOL)
