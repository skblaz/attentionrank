## synthatic data.

from sklearn.datasets import make_classification


## for each synthetic dataset, find the topk, k in informative.
X,Y = make_classification(n_samples = 1000, n_features = 20, n_informative = 5, n_redundant = 2))
