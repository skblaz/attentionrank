import os
import pickle
from utilss import find_target_key_n_features


def csv_to_arff_simple(csv, arff):
    """
    Converts csv to arff where it is assumed that class is the last attribute and all attributes are numeric
    :param csv:
    :return:
    """
    raise RuntimeError("Some of the data break the assumption ...")
    with open(csv) as f:
        header = f.readline().strip().split(',')
        matrix = [line.strip().split(',') for line in f.readlines()]
    class_values = "{{{}}}".format(','.join(sorted(set([example[-1] for example in matrix]))))
    with open(arff, "w") as f:
        print("@relation {}".format(csv), file=f)
        for a in header[:-1]:
            print("@attribute {} numeric".format(a), file=f)
        print("@attribute {} {}".format(header[-1], class_values), file=f)
        print("@data", file=f)
        for line in matrix:
            print(','.join(line), file=f)


def convert_all():
    data = "../data"
    for csv in os.listdir(data):
        if "Frogs_MFCC" in csv or "annealing" in csv or not csv.endswith(".csv"):
            continue
        in_file = os.path.join(data, csv)
        out_file = os.path.join(data, csv + ".arff")
        csv_to_arff_simple(in_file, out_file)


# convert_all()


S = """[General]
ResourceInfoLoaded = Yes
Verbose = 1

[Data]
File = ../data/arff_datasets/{0}.arff

[Attributes]
{1}
{2}


[Ensemble]
Iterations = 100
NumberOfThreads = 1
EnsembleMethod = RForest
FeatureRanking = Genie3
SelectRandomSubspaces = SQRT
Optimize = No
PrintAllModels = No
PrintAllModelFiles = No
PrintAllModelInfo = No
PrintPaths = No
OOBestimate = No

[Relief]
Neighbours = 15

[Output]
WritePredictions = [Test]
"""


def prepare_experiments():
    experiment_dir = "../experiments_relief_genie/"
    command = "java -Xmx20G -jar clus.jar -{} {} >> output.log 2>>&1"
    data = "../data/arff_datasets"
    commands = []
    for arff in os.listdir(data):
        if not arff.endswith("arff"):
            continue
        target_str, key_str = find_target_key_n_features(os.path.join(data, arff))
        data_set = arff[:arff.find('.')]
        for ranking in ["forest", "relief"]:
            s_name = "{}_{}.s".format(data_set, ranking)
            with open(experiment_dir + s_name, "w") as f:
                print(S.format(data_set, target_str, key_str), file=f)
            commands.append(command.format(ranking, s_name))
    with open("{}/run.bat".format(experiment_dir), "w") as f:
        print('\n'.join(commands), file=f)


# prepare_experiments()


def convert_fimp_to_pickle(fimp):
    data_set = os.path.basename(fimp)
    data_set = data_set[:data_set.find('_')]
    feature_importance = []
    column_names = []
    top_acc = None
    method = "forest" if "Genie3" in fimp else "relief"
    top_setting = {"relief": ["15", "all"], "forest": ["RForest", "Genie3", "100"]}
    output_file = '../results/{}_{}.pickle'.format(method, data_set)
    print("Pickling", fimp)
    if os.path.exists(output_file):
        print("    Skipping", fimp)
        return 21
    with open(fimp) as f:
        for l in f:
            if l.startswith("-------"):
                break
        for l in f:
            line = l.strip().split('\t')
            column_names.append(line[1])
            feature_importance.append(eval(line[3])[0])
    out_result = {"importances": feature_importance, "names": column_names,
                  "dataset": data_set,
                  "top_acc": top_acc,
                  "top_param": top_setting[method], "method": method}
    with open(output_file, 'wb') as handle:
        pickle.dump(out_result, handle, protocol=pickle.HIGHEST_PROTOCOL)


def convert_fimps_to_pickle():
    exp_dir = "../experiments_relief_genie"
    for fimp in os.listdir(exp_dir):
        if not fimp.endswith(".fimp"):
            continue
        fimp_path = os.path.join(exp_dir, fimp)
        convert_fimp_to_pickle(fimp_path)


# convert_fimps_to_pickle()
convert_fimp_to_pickle("../experiments_relief_genie/p53_relief.fimp")
