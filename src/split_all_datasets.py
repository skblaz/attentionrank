import glob
from divide import stratify_wrapper
import os
from prepare_datasets import arff_to_csv
all_arff_datasets = glob.glob("../data/arff_datasets/*")

os.system("rm -rvf ../data/stratified/*; rm -rvf ../data/csv_datasets/*")

## generate folds
for dx in all_arff_datasets:
    print("Splitting:",dx)
    arff_to_csv(dx, "../data/csv_datasets/")
    stratify_wrapper(dx)

all_csv_datasets = glob.glob("../data/csv_datasets/*")
for dx in all_csv_datasets:    
    print("Splitting:", dx)
    stratify_wrapper(dx)
