## evaluate rankings based on classification
import glob
from rank_module import *
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import f1_score, accuracy_score
from sklearn.neighbors import KNeighborsClassifier
from divide import stratify_wrapper
from prepare_datasets import arff_to_csv
from utilss import *
from sympy import sieve
import tqdm
from nn_classifier import *
import os

import numpy

numpy.warnings.filterwarnings('ignore')
import logging

logging.basicConfig(format='%(asctime)s - %(message)s', datefmt='%d-%b-%y %H:%M:%S')
logging.getLogger().setLevel(logging.INFO)

from sklearn import linear_model

S_PATTERN_KNN_WEIGHTS = """[General]
ResourceInfoLoaded = Yes
Verbose = 0

[Data]
File = {0}
TestSet = {1}

[Attributes]
{3}
{4}

[kNN]
K = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 20, 25, 30]
Distance = Euclidean
SearchMethod = BruteForce
DistanceWeighting = [Constant]
AttributeWeighting = {2}

[Output]
TrainErrors = No
WritePredictions = [Test]"""

S_PATTERN_TOP_INDICES = """[General]
ResourceInfoLoaded = Yes
Verbose = 0

[Data]
File = {0}
TestSet = {1}

[Attributes]
Descriptive = {2}
Target = {3}

[Ensemble]
Iterations = 100
NumberOfThreads = 1
EnsembleMethod = RForest
SelectRandomSubspaces = SQRT
Optimize = No
PrintAllModels = No
PrintAllModelFiles = No
PrintAllModelInfo = No
PrintPaths = No
OOBestimate = No
SortRankingByRelevance = No

[Output]
TrainErrors = No
WritePredictions = [Test]"""


class ClusRF:
    def __init__(self, indices_used, target_index, train_arff, test_arff):
        self.target_index = target_index + 1  # 0-based to 1-based
        self.used_indices = [i + 1 for i in indices_used]  # 0-based to 1-based
        self.train = train_arff
        self.test = test_arff
        self.predictions = None

    def fit(self, *args):
        arffs_absolute = [os.path.abspath(x) for x in [self.train, self.test]]
        temp_dir = ""
        for i in range(1, 10**3):
            temp_dir = os.path.abspath("temp_eval_top_indices{}".format(i))
            if not os.path.exists(temp_dir):
                os.makedirs(temp_dir)
        if not temp_dir:
            raise ValueError("...")
        # experiment data
        descriptive = ",".join([str(i) for i in self.used_indices])
        target = self.target_index
        s_file = os.path.join(temp_dir, "experiment.s")
        with open(s_file, "w", newline="") as f:
            print(S_PATTERN_TOP_INDICES.format(arffs_absolute[0], arffs_absolute[1],
                                               descriptive, target),
                  file=f)
        # run the experiment
        clus = os.path.abspath("../experiments_relief_genie/clus.jar")
        subprocess.call("java -Xmx10G -jar {} -forest {}".format(clus, s_file),
                        shell=True, stderr=stderr, stdout=stdout)
        # process the results
        prediction_file = s_file[:s_file.rfind(".")] + ".test.pred.arff"
        # skip true values
        self.predictions = get_classification_predictions(prediction_file)[1:]
        shutil.rmtree(temp_dir)

    def predict(self, x=None):
        return self.predictions[0]


def get_classification_predictions(prediction_file):
    def parse_header():
        n_models = sum(x.endswith("string") for x in header)
        lines_per_model = (len(header) - 1) // n_models
        # n_target_values = lines_per_model - 2
        return n_models, lines_per_model

    header = []
    with open(prediction_file) as f:
        for l in f:
            if l.lower().startswith("@data"):
                break
            elif l.lower().startswith("@attribute"):
                header.append(l.strip())
        models, lines = parse_header()
        values = [[] for _ in range(1 + models)]
        for l in f:
            line = l.strip().split(',')
            values[0].append(line[0])
            for i in range(models):
                values[i + 1].append(line[1 + i * lines])
        return values


def evaluate_rankings_on_test(train_set, test_set, rankings, dataset, fold, train_arff, test_arff, name=None):
    def create_classifier(classifier, indices_used, t_index):
        if classifier == "LR":
            return LogisticRegression()
        elif classifier == "KNN":
            return KNeighborsClassifier(5)
        elif classifier == "DNN":
            return e2e_DNN()
        elif classifier == "SGD":
            return linear_model.SGDClassifier(max_iter=1000, tol=1e-3)
        elif classifier == "RF":
            return ClusRF(indices_used, t_index, train_arff, test_arff)
        else:
            return None

    train_instances_real, target_train = load_and_process_generic(train_set)
    test_instances_real, target_test = load_and_process_generic(test_set)
    out_tuplets = []
    #    {"importances":feature_importances,"names":cnames,"dataset":dataset,"top_acc":top_acc,"top_param":top_setting,"method":"xgb"}
    classifiers = ["LR"]  # , "KNN", "SGD", "RF"]
    # all_indices = list(range(len(rankings["importances"])))
    target_index, key_index, n_features = find_target_key_n_features(train_arff, False)
    all_indices = compute_feature_indices(n_features, target_index, key_index, True)
    target_index -= 1
    #    key_index -= 1  # not necessary (it's never used ...)

    baseline_scores = {c: {"acc": None, "f1": None} for c in classifiers}
    for c in classifiers:
        clf = create_classifier(c, all_indices, target_index)
        clf.fit(train_instances_real, target_train)
        preds = clf.predict(test_instances_real)
        try:
            preds = [int(x) for x in preds]
        except Exception as es:
            pass
        baseline_scores[c]["acc"] = accuracy_score(preds, target_test)
        baseline_scores[c]["f1"] = f1_score(preds, target_test, average="micro")


    rangex = []
    for j in range(30):

        if 2**j < train_instances_real.shape[1]:

            rangex.append(2**j)

#   for i in sieve.primerange(2, train_instances_real.shape[1]):
#       rangex.append(int(i))

    rangex.append(int(train_instances_real.shape[1]))
    out_tuplets.append((int(train_instances_real.shape[1]), "LR", 1.00, 1.00, dataset.split("/")[-1], rankings['method'], fold))
    stopping_accuracy = 0
    current_acc = 0
    terminate = False
    for prime in tqdm.tqdm(rangex):

        if stopping_accuracy == 20: ## converged.
            logging.info("Stopping")
            prime = rangex[-1]
            terminate = False ## after this finishes.

        if name:
            logging.info("Checking top {} features for {}".format(prime, name))

        else:
            logging.info("Checking top {} features".format(prime))

        top_indices = np.array(rankings['importances']).argsort()[-prime:][::-1]
        used_feature_indices = [all_indices[f_index] for f_index in top_indices]
        top_features = [f for en, f in enumerate(rankings['names']) if en in top_indices]
        train_instances_tmp = train_instances_real[top_features]
        for c in classifiers:
            clf = create_classifier(c, used_feature_indices, target_index)
            clf.fit(train_instances_tmp, target_train)
            preds = clf.predict(test_instances_real[top_features])
            try:
                preds = [int(x) for x in preds]
            except Exception as es:
                pass
            accuracy_fs = accuracy_score(preds, target_test)
            f1_fs = f1_score(preds, target_test, average="micro")
            accuracy = accuracy_fs / baseline_scores[c]["acc"]
            if accuracy != current_acc:
                current_acc = accuracy
            else:
                stopping_accuracy += 1
            f1 = f1_fs / baseline_scores[c]["f1"]
            out_tuplets.append((prime, c, f1, accuracy, dataset.split("/")[-1], rankings['method'], fold))

        # if terminate:
        #     break

    return out_tuplets


def evaluate_via_dimension_weights(train_set, test_set, ranking):
    def compute_metrics():
        values = get_classification_predictions(prediction_file)
        true_values, predictions = values[0], values[1:]
        accuracies, f1 = [], []
        for prediction in predictions:
            accuracies.append(accuracy_score(true_values, prediction))
            f1.append(f1_score(true_values, prediction, average="micro"))
        return accuracies, f1

    def normalize_importance(importances, f_indices):
        # basic = [max(0, importance) for importance in importances]
        assert len(f_indices) == len(importances)
        normalized = [0.0] * (f_indices[-1] + 1)
        for f_index, importance in zip(f_indices, importances):
            normalized[f_index] = max(0, importance)
        return normalized

    arffs_absolute = [os.path.abspath(x) for x in [train_set, test_set]]
    temp_dir = os.path.abspath("temp_eval")
    if not os.path.exists(temp_dir):
        os.makedirs(temp_dir)
    # experiment data
    target_index, key_index, n_features = find_target_key_n_features(arffs_absolute[0], False)
    feature_indices = compute_feature_indices(n_features, target_index, key_index, True)
    importances_normalized = normalize_importance(ranking["importances"], feature_indices)
    s_file = os.path.join(temp_dir, "experiment.s")

    target_str, key_str = find_target_key_n_features(arffs_absolute[0])

    with open(s_file, "w", newline="") as f:
        print(S_PATTERN_KNN_WEIGHTS.format(arffs_absolute[0], arffs_absolute[1], importances_normalized,
                                           target_str, key_str), file=f)
    # run the experiment
    clus = os.path.abspath("../experiments_relief_genie/clus.jar")
    subprocess.call("java -Xmx10G -jar {} -knn {}".format(clus, s_file),
                    shell=True, stderr=stderr, stdout=stdout)
    # process the results
    prediction_file = s_file[:s_file.rfind(".")] + ".test.pred.arff"
    a, f = compute_metrics()
    shutil.rmtree(temp_dir)
    # return acc, micro-f1s and k from nearest neighbours
    return a, f, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 20, 25, 30]


def save_ranking_to_pickle(f_name, ranking):
    dir_name = os.path.dirname(f_name)
    os.makedirs(dir_name, exist_ok=True)
    with open(f_name, 'wb') as handle:
        pickle.dump(ranking, handle, protocol=pickle.HIGHEST_PROTOCOL)


if __name__ == "__main__":

    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("--result_folder", default="../final_results_new_data")
    allowed_data = []
    args = parser.parse_args()

    skip_attention = False

    num_folds = 10
    for j in range(num_folds):
        print("Parsing fold {}".format(j))
        folds_folder = "../data/stratified/fold{}/*".format(j + 1)
        all_datasets = []
        for fname in glob.glob(folds_folder):
            all_datasets.append(fname.split("_")[0])
        all_datasets = set(all_datasets)
        for dataset in all_datasets:
            logging.info("Evaluation {}, fold {}".format(dataset, j))
            train_data = dataset + "_train.csv"
            test_data = dataset + "_test.csv"

            train_data_arff = dataset + "_train.arff"
            test_data_arff = dataset + "_test.arff"

            dataset_name = os.path.basename(dataset)
            ranking_by_folds = '../rankings_by_folds/'
            pickle_pattern = "{}{{}}_{{}}_{}.pickle".format(ranking_by_folds, dataset_name)

            if not skip_attention:
                # attention
                logging.info("Attention1 ..")
                attention_rankings_train, attention_rankings_clean_train, self_attention_train = attention_rankings(
                    train_data)

                save_ranking_to_pickle(pickle_pattern.format("attention", j), attention_rankings_train)
                attention_results = evaluate_rankings_on_test(train_data, test_data, attention_rankings_train, dataset,
                                                              j,
                                                              train_data_arff, test_data_arff)
                logging.info("Attention2 ..")
                save_ranking_to_pickle(pickle_pattern.format("attentionClean", j), attention_rankings_clean_train)
                attention_clean_results = evaluate_rankings_on_test(train_data, test_data,
                                                                    attention_rankings_clean_train,
                                                                    dataset, j, train_data_arff, test_data_arff)
                logging.info("Attention3 ..")
                save_ranking_to_pickle(pickle_pattern.format("attentionLayer", j), self_attention_train)
                attention_layer_results = evaluate_rankings_on_test(train_data, test_data, self_attention_train,
                                                                    dataset, j,
                                                                    train_data_arff, test_data_arff)

            # SHAP
            # shap_rankings_train = shap_rankings(train_data)
            # print(shap_rankings_train)
            # with open('../rankings_by_folds/' + "shap_" + str(j) + "_" + dataset.split("/")[-1] + ".pickle",
            #           'wb') as handle:
            #     pickle.dump(shap_rankings_train, handle, protocol=pickle.HIGHEST_PROTOCOL)

            # shap_results = evaluate_rankings_on_test(train_data, test_data, shap_rankings_train, dataset, j,
            #                                          train_data_arff, test_data_arff)

            # # mutual info
            # logging.info("Mutual Info ..")
            # MI_rankings_train = mutual_info_rankings(train_data)
            # save_ranking_to_pickle(pickle_pattern.format("MI", j), MI_rankings_train)
            # MI_results = evaluate_rankings_on_test(train_data, test_data, MI_rankings_train, dataset, j,
            #                                        train_data_arff, test_data_arff, name="Mutual Info")

            # # Genie3
            # logging.info("Genie3 ..")
            # genie_ranking_train = genie_ranking(train_data_arff)
            # genie_results = evaluate_rankings_on_test(train_data, test_data, genie_ranking_train, dataset, j,
            #                                           train_data_arff, test_data_arff, name="genie3")

            # # Relief
            # logging.info("ReliefF ..")
            # relief_ranking_train = relief_ranking(train_data_arff)
            # relief_results = evaluate_rankings_on_test(train_data, test_data, relief_ranking_train, dataset, j,
            #                                            train_data_arff, test_data_arff, name="relief")

            # combine and print rankings
            # (shap_rankings_train, "shap")
            # rankings_train = [(attention_rankings_train, "Attention"),
            #                   (genie_ranking_train, "genie"), (relief_ranking_train, "relief"),
            #                   (MI_rankings_train, "MI"), (attention_rankings_clean_train, "AttentionPositive"),
            #                   (self_attention_train, "AttentionLayer")]
            rankings_train = [(relief_ranking_train, "relief")]
            all_results = relief_results  # attention_results + genie_results + relief_results + MI_results + attention_clean_results + attention_layer_results  # + shap_results
            result_line = (
                "\n".join("RESULT_LINE" + "\t" + "\t".join([str(x) for x in result]) for result in all_results))
            logging.info(result_line)
            os.makedirs(args.result_folder, exist_ok=True)
            fx = open(args.result_folder + "/ranking_results_attention_only.txt", "a")
            fx.write(result_line)
            fx.close()

            # Evaluation via kNN weights
            for ranking_train in rankings_train:
                a, f, rx = evaluate_via_dimension_weights(train_data_arff, test_data_arff, ranking_train[0])
                pdx = pd.DataFrame()
                pdx['TAG'] = "KNN_TESTING_DATAFRAME"
                pdx['accuracy'] = a
                pdx['f1'] = f
                pdx['range'] = rx
                pdx['name'] = ranking_train[1]
                with open(args.result_folder + "/knn_results_att.txt", 'a') as f:
                    pdx.to_csv(f, header=False)

                    ## clean: rm -rvf ../rank_evaluation/* && rm -rvf ../rankings_by_folds/*
