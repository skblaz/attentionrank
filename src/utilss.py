def find_target_key_n_features(arff, create_string=True):
    """
    The hord-codedest function, I know.
    :param arff:
    :param create_string
    :return:
    """
    n_attributes = 0
    id_attribute = None
    target_attribute = None
    with open(arff) as f:
        for l in f:
            if l.lower().startswith("@attribute"):
                n_attributes += 1
            if l.lower().startswith("@attribute class"):
                target_attribute = n_attributes
            elif l.lower().startswith("@attribute id"):
                id_attribute = n_attributes
            elif l.lower().startswith("@data"):
                break
    assert target_attribute is not None
    target_str = "Target = {}".format(target_attribute)
    key_str = "Key = {}".format(id_attribute) if id_attribute is not None else ""
    if create_string:
        return target_str, key_str
    else:
        return target_attribute, id_attribute, n_attributes - 1 - (id_attribute is not None)


def compute_feature_indices(n_features, target_index, id_index, convert_to_0_based):
    """
    5, 1, 2 ---> [3, 4, 5, 6, 7]
    5, 1, None ---> [2, 3, 4, 5, 6]
    5, 6, None ---> [1, 2, 3, 4, 5]
    5, 7, 1 ---> [2, 3, 4, 5, 6]

    :param n_features:
    :param target_index: 1-based
    :param id_index: 1-based
    :return:
    """
    feature_indices = list(range(1, n_features + 1))
    for i, j in enumerate(feature_indices):
        offset = (target_index <= j) + (id_index is not None and id_index <= j) - convert_to_0_based
        feature_indices[i] += offset
    return feature_indices
