try:
    import torch
    import torch.nn as nn
    from torch.utils.data import DataLoader, Dataset
except ImportError:
    print("torch ...")

class DatasetLoaderGeneric(Dataset):
    
    def __init__(self, file_path, labels, transform=None):
        self.data = file_path
        self.labels = labels
        
    def __len__(self):
        return len(self.data)
    
    def __getitem__(self, index):
        image = self.data[index, :]
        label = self.labels[index]            
        return image, label

class NeuralNet(nn.Module):
    def __init__(self, input_size, hidden_size, num_classes, dropout=0.2):
        super(NeuralNet, self).__init__()
        self.fc1 = nn.Linear(input_size, input_size)
        self.softmax = nn.Softmax(dim=1)
        self.relu = nn.ReLU()
        self.dropout = nn.Dropout(dropout)
        self.fc2 = nn.Linear(input_size, hidden_size)
        self.fc3 = nn.Linear(hidden_size, num_classes)
        
    def forward(self, x):
        out = self.fc1(x)
        out = self.softmax(out)
        out = out * x
        out = self.fc2(out)
        out = self.dropout(out)
        out = self.fc3(out)
        return out
    
    def get_attention(self,x):
        out = self.fc1(x)
        out = self.softmax(out)
        return out
    
    def get_hadamand_layer(self):
        return self.fc1.weight.data
    
    def get_softmax_hadamand_layer(self):
        return self.softmax(self.fc1.weight.data)
