import random
import os


def stratify(arff, target_index, folds, out_dir, random_seed):
    """
    Create stratified cross-validation data sets.
    :param arff: path/to/file.arff or path/to/file.csv
    :param target_index: >= 0
    :param folds: 10 or something
    :param out_dir: New files appear in the directories out_dir/fold<n>, where 1 <= n <= folds
    :return:
    """

    def razdeli_na_porcije(n, k):
        osnovna_porcija = n // k
        ostanek = n % k
        return [osnovna_porcija + (ostanek > i) for i in range(k)], ostanek

    def porcije2intervali(porcije):
        """[4,4,3] ---> [[0, 4), [4, 8), [8, 11)]"""
        intervali = [[0, porcije[0]]]  # fold: [od, do)
        for fold in range(1, len(porcije)):
            intervali.append([intervali[-1][1], intervali[-1][1] + porcije[fold]])
        return intervali

    def intervali2trainTestRange(intervali, fold):
        train, test = [], []
        for fol in range(len(intervali)):
            for k in range(intervali[fol][0], intervali[fol][1]):
                if fol == fold:
                    test.append(k)
                else:
                    train.append(k)
        return train, test

    glava = []
    podatki = []
    pripadniki = {}
    with open(arff) as f:
        if arff.endswith(".arff"):
            for x in f:
                glava.append(x.strip())
                if "@data" in x.lower():
                    break
        else:
            glava.append(f.readline().strip())
        primerek = 0
        for x in f:
            a = x.strip()
            if a:  # prazne vrste
                vrstica = [feat.strip() for feat in a.split(",")]
                if vrstica[target_index] not in pripadniki:
                    pripadniki[vrstica[target_index]] = []
                pripadniki[vrstica[target_index]].append(primerek)
                primerek += 1
                podatki.append(a)
    primerov_na_fold, _ = razdeli_na_porcije(len(podatki), folds)

    se_prostora = primerov_na_fold[::]
    zamik = 0
    razredi = sorted(pripadniki.keys(), key=lambda x: -len(pripadniki[x]))  # v resnic je vazn sam, da je vrstni red fix
    kolk_kam = [[None for _ in range(len(razredi))] for _ in range(folds)]
    for i, razred in enumerate(razredi):
        raz_na_fold, ost = razdeli_na_porcije(len(pripadniki[razred]), folds)
        for fold in range(folds):
            ind_fold = (fold + zamik) % folds
            kolk_kam[ind_fold][i] = raz_na_fold[fold]
            se_prostora[ind_fold] -= raz_na_fold[fold]
        zamik = (zamik + ost) % folds
    assert not any(se_prostora)  # <==> vsi so enaki 0

    random.seed(random_seed)
    for i, razred in enumerate(razredi):
        random.shuffle(pripadniki[razred])  # tu je edini random ...

    file_name = os.path.basename(arff)
    file_pure = file_name[:file_name.find('.')]
    file_end = file_name[file_name.rfind('.'):]
    for fold in range(folds):
        resnaOutMapa = "{}/fold{}".format(out_dir, fold + 1)
        if not os.path.exists(resnaOutMapa):
            os.makedirs(resnaOutMapa)
        arffs = [os.path.join(resnaOutMapa, "{}_{}{}".format(file_pure, appendix, file_end))
                 for appendix in ["train", "test"]]
        for a in range(2):
            with open(arffs[a], "w", newline='') as f:
                for vrsta in glava:
                    print(vrsta, file=f)
                for i, razred in enumerate(razredi):
                    razdelitev = porcije2intervali([kolk_kam[fold][i] for fold in range(folds)])
                    ranges = intervali2trainTestRange(razdelitev, fold)
                    for primerek in ranges[a]:
                        print(podatki[pripadniki[razred][primerek]], file=f)


# def divide_all():
#     for f in os.listdir('.'):
#         if not any([f.startswith(x) for x in ["Frogs", "annealing"]]) and (f.endswith(".arff") or f.endswith(".csv")):
#             stratify(f, -1, 10, "stratified/", 12321)


def stratify_wrapper(data_file):
    # find class index
    if data_file.endswith("csv"):
        with open(data_file) as f:
            header = f.readline().strip().split(',')
            target = header.index("class")
    else:
        attributes = 0
        target = None
        with open(data_file) as f:
            for l in f:
                if l.startswith("@attribute class"):
                    target = attributes
                    break
                elif l.startswith("@attribute"):
                    # divide_all()
                    attributes += 1
        assert target is not None
    stratify(data_file, target, 10, "../data/stratified", 12321)
