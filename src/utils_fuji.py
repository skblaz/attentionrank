from typing import List, Union, Dict, Tuple
from tqdm import trange

JACCARD = "jaccard"
FUZZY_JACCARD = "fuzzy_jaccard"
ALLOWED_MEASURES = [JACCARD, FUZZY_JACCARD]

STEP_1 = 1
STEP_SQUARED = "squared"
STEP_EXP = "exp"
ALLOWED_STEPS = [STEP_1, STEP_SQUARED, STEP_EXP]

IMPORTANCE_HANDLER_RAISE = "raise"
IMPORTANCE_HANDLER_CORRECT = "correct"
ALLOWED_IMPORTANCE_HANDLERS = [IMPORTANCE_HANDLER_RAISE, IMPORTANCE_HANDLER_CORRECT]


class Fimp:
    def __init__(self, feature_dictionary: Dict[str, Tuple[int, List[int], List[float]]]):
        """
        Creates a feature importance structure.
        :param feature_dictionary: A dictionary of the form
         {feature name: (feature index, feature ranks, feature importances)}. The lengths of the last two items in each
         value triplet are the same, and they correspond to the number of feature rankings stored in the Fimp structure.
          Taking i-th components of these two lists, gives us the ranks and importances from the i-th feature ranking,
          for all features.
        """
        self.table = []  # [[dataset index, name, ranks, relevances], ...]
        self.features = {}  # {name: [dataset index, ranks, relevances], ...}
        self.features = feature_dictionary
        for attr in feature_dictionary:
            row = [feature_dictionary[attr][0], attr, feature_dictionary[attr][1], feature_dictionary[attr][2]]
            self.table.append(row)


    def sort_by_feature_index(self):
        self.table.sort(key=lambda row: row[0])

    def sort_by_relevance(self, ranking_index=0):
        self.table.sort(key=lambda row: row[2][ranking_index])

    def get_feature_names(self):
        return [row[1] for row in self.table]

    def get_relevances(self, ranking_index=None):
        return [row[-1] if ranking_index is None else row[-1][ranking_index] for row in self.table]

    def get_relevance(self, feature_name, ranking_index=None):
        i = 0 if ranking_index is None else ranking_index
        return self.features[feature_name][-1][i]

    def set_relevances(self, ranking_index, feature_relevances):
        """Overwrites the current feature relevances. Does not recompute the ranks."""
        assert len(feature_relevances) == len(self.table)
        for i, (_, name, _, _) in enumerate(self.table):
            self.table[i][-1][ranking_index] = feature_relevances[i]
            self.features[name][-1][ranking_index] = feature_relevances[i]

    @staticmethod
    def create_fimp_from_relevances(feature_relavance_scores,
                                    feature_names: Union[List[str], None] = None,
                                    feature_indices: Union[List[int], None] = None):
        n = len(feature_relavance_scores)
        if feature_indices is None:
            feature_indices = [i + 1 for i in range(n)]
        if feature_names is None:
            assert isinstance(feature_indices, list)
            feature_names = ["a{}".format(i) for i in feature_indices]
        # compute ranks
        ranks = [-1 for _ in range(n)]
        relevances_positions = list(zip(feature_relavance_scores, range(n)))
        relevances_positions.sort(reverse=True)
        rank = 0
        for i, relevance_position in enumerate(relevances_positions):
            relevance, position = relevance_position
            if i == 0 or abs(relevance - relevances_positions[i - 1][0]) > 10 ** -12:
                rank = i + 1
            ranks[position] = rank
        d = {a: (i, [rank], [relevance]) for a, i, rank, relevance in zip(feature_names,
                                                                          feature_indices,
                                                                          ranks,
                                                                          feature_relavance_scores)}
        return Fimp(feature_dictionary=d)


def compute_similarity_helper(fimp1: Fimp, fimp2: Fimp, similarity_measure: str, eps: float,
                        step: Union[str, int], use_tqdm: bool, negative_importances_handler: str):
    def jaccard(f1: Fimp, f2: Fimp):
        for f in [f1, f2]:
            f.sort_by_relevance(ranking_index)
        attributes1 = f1.get_feature_names()
        attributes2 = f2.get_feature_names()
        results = [-1.0] * len(attributes1)
        intersection = set()
        union = set()
        for i in range(len(attributes1)):
            a1, a2 = attributes1[i], attributes2[i]
            if a1 == a2:
                intersection.add(a1)
                union.add(a1)
            else:
                if a1 in union:
                    intersection.add(a1)  # a1 has been added as part of the attributes2 before
                if a2 in union:
                    intersection.add(a2)  # symmetric case
                union.add(a1)
                union.add(a2)
            results[i] = len(intersection) / len(union)
        return results

    def fuzzy_jaccard(f1: Fimp, f2: Fimp):
        def relative_score(absolute_score, normalisation_factor):
            if normalisation_factor == 0.0:
                return 1.0
            else:
                return absolute_score / normalisation_factor

        for f in [f1, f2]:
            f.sort_by_relevance(ranking_index)
        attributes = [f1.get_feature_names(), f2.get_feature_names()]
        n = len(attributes[0])

        for f in [f1, f2]:
            feature_relevances = f.get_relevances(ranking_index)
            if min(feature_relevances) < 0:
                if negative_importances_handler == "raise":
                    raise ValueError("Feature importances must not be negative")
                elif negative_importances_handler == "correct":
                    non_negative = [max(0, r) for r in feature_relevances]
                    f.set_relevances(ranking_index, non_negative)
                    feature_relevances = f.get_relevances(ranking_index)
                else:
                    raise ValueError("Wrong negative importances handling: {}".format(negative_importances_handler))
            if not all([float("-inf") < x < float("inf") for x in feature_relevances]):
                raise ValueError("Not all feature importances are finite numbers.")

        if isinstance(step, str):
            feature_subset_sizes = []
            if step == "exp":
                i = 1
                while i <= n:
                    feature_subset_sizes.append(i - 1)
                    i *= 2
            elif step == "squared":
                i = 1
                while i ** 2 <= n:
                    feature_subset_sizes.append(i ** 2 - 1)
                    i += 1
            else:
                raise ValueError("Wrong step specification: {}".format(step))
        else:
            feature_subset_sizes = list(range(0, n, step))
        if feature_subset_sizes[-1] != n - 1:
            feature_subset_sizes.append(n - 1)

        n_evaluated_subsets = len(feature_subset_sizes)
        i_subset = 0
        results = [-1.0] * n_evaluated_subsets
        min_scores = [float("inf")] * 2
        union_set = set()  # more exactly, union - intersection
        intersection_set = set()
        iterator = trange(n) if use_tqdm else range(n)
        for i in iterator:
            for j, (attributes_ranking, f) in enumerate(zip(attributes, [f1, f2])):
                feature = attributes_ranking[i]
                s = f.get_relevance(feature, ranking_index)
                min_scores[j] = min(min_scores[j], s)
                if max(min_scores) <= eps:
                    for i1 in range(i_subset, n_evaluated_subsets):
                        results[i1] = 1.0
                    return results
                if feature in union_set:
                    union_set.remove(feature)
                    intersection_set.add(feature)
                else:
                    union_set.add(feature)
            if i != feature_subset_sizes[i_subset]:
                continue
            fuzzy_intersection = len(intersection_set)
            for feature in union_set:
                s1 = min(1.0, relative_score(f1.get_relevance(feature, ranking_index), min_scores[0]))
                s2 = min(1.0, relative_score(f2.get_relevance(feature, ranking_index), min_scores[1]))
                fuzzy_intersection += min(s1, s2)
            fuzzy_union = len(intersection_set) + len(union_set)
            results[i_subset] = fuzzy_intersection / fuzzy_union
            i_subset += 1
        return results

    ranking_index = 0
    # sanity check
    for fimp in [fimp1, fimp2]:
        fimp.sort_by_feature_index()
    if fimp1.get_feature_names() != fimp2.get_feature_names():
        raise ValueError("Names of the attributes are not the same")
    if similarity_measure == JACCARD:
        return jaccard(fimp1, fimp2)
    elif similarity_measure == FUZZY_JACCARD:
        return fuzzy_jaccard(fimp1, fimp2)
    else:
        raise ValueError("Chosen error measure {} is not among the allowed ones: {}".format(similarity_measure,
                                                                                            ALLOWED_MEASURES))


def area_under_the_curve(points):
    n = len(points) - 1
    a = 0.0
    for i in range(n):
        a += (points[i] + points[i + 1]) / 2
    return a / n



