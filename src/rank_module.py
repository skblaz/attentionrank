### ranking by attention
import pandas as pd
from sklearn import preprocessing
from sklearn.model_selection import StratifiedKFold
from sklearn.feature_selection import mutual_info_classif
from sklearn.metrics import accuracy_score
import logging
import numpy as np
logging.basicConfig(format='%(asctime)s - %(message)s', datefmt='%d-%b-%y %H:%M:%S')
logging.getLogger().setLevel(logging.INFO)
try:
    import xgboost as xgb
    import shap
except:
    pass
import torch
import torch.nn as nn
import numpy as np
    
# from arcs import *
#import matplotlib.pyp
#import seaborn as sns
import pickle
import os
import shutil
import tqdm
import subprocess
from sys import stdout, stderr
import re
from attentionRank import simple_ffNN
from utilss import find_target_key_n_features


def load_and_process_generic(dname):
    data = pd.read_csv(dname, sep=",", na_values = "?")
    data = data.fillna(data.mean())
    if "class" in data.columns:
        cols = [x for x in data.columns if x not in ["class", "id"]]
        logging.info("Found {} features".format(len(cols)))
        if "id" in cols:
            print("ID still in cols?")
        train_data = data[cols]
        target = data[["class"]]
#        train_data = train_data.dropna(axis='columns')
        return train_data, target
    else:
        raise ValueError("Man, you need to have a target class variable ...")
        # return (None, None)

def simple_xgb(train_features, test_features=None, train_tars=None, test_tars=None, grid=[100, 20, 0.02, 10, 20, 10],
               explainer=False):
    param = {}
    param['estimators'] = grid[0]
    param['max_depth'] = grid[1]
    param['learning_rate'] = grid[2]
    param['objective'] = 'multi:softprob'
    param['min_child_weight'] = grid[3]
    param['gamma'] = grid[4]
    param['calsample_bytree'] = grid[5]
    param['verbose_eval'] = 50
    param['num_class'] = np.max(train_tars) + 1
    clf = xgb.train(param, xgb.DMatrix(train_features, label=train_tars))
    if explainer:
        explainer = shap.TreeExplainer(clf)
        print(train_features.shape)
        shap_values = explainer.shap_values(train_features)[0]
        shap_values = shap_values[:, 0:train_features.shape[1]]
        #        shap.summary_plot(shap_values, train_features)
        #        plt.show()
        return shap_values
    else:
        assert set(test_features.columns) == set(train_features.columns)
        test_features = xgb.DMatrix(test_features)
        preds = clf.predict(test_features)
        preds = np.argmax(preds, axis=1)
        return accuracy_score(preds, test_tars)


def shap_rankings(dataset):
    train_instances_real, target_first = load_and_process_generic(dataset)
    enc0 = preprocessing.LabelEncoder()
    enc1 = preprocessing.OneHotEncoder()
    target_first_encoded = enc0.fit_transform(target_first)
    skf = StratifiedKFold(n_splits=3)

    train_instances = train_instances_real
    overall_acc = []

    xgb_grid = []
    
    grid_xgb = {"learning_rate"    : [0.05, 0.30 ] ,
                "max_depth"        : [ 3, 10, 15],
                "min_child_weight" : [ 1,  5 ],
                "gamma"            : [ 0.0, 0.1, 0.2 , 0.3, 0.4 ],
                "estimators"            : [100,300],
    "colsample_bytree" : [ 0.3, 0.7 ] }

    for e in grid_xgb['estimators']:
        for d in grid_xgb['max_depth']:
            for lr in grid_xgb['learning_rate']:
                for mcw in grid_xgb['min_child_weight']:
                    for g in grid_xgb['gamma']:
                        for cs in grid_xgb['colsample_bytree']:
                            xgb_grid.append([e, d, lr, mcw, g, cs])

    top_acc = 0
    top_setting = None
    for parameter_setting in xgb_grid:
        print("Testing {} setting.".format(parameter_setting))
        for train_index, test_index in skf.split(train_instances, target_first_encoded):
            train_features = train_instances.iloc[train_index, :]
            test_features = train_instances.iloc[test_index, :]
            assert set(train_features.columns) == set(test_features.columns)
            train_tars = target_first_encoded[train_index]
            test_tars = target_first_encoded[test_index]
            acc = simple_xgb(train_features, test_features, train_tars, test_tars, parameter_setting)
            overall_acc.append(acc)
        acc = np.mean(overall_acc)
        if top_acc < acc:
            print("Improved accuracy to {}%".format(acc))
            top_acc = acc
            top_setting = parameter_setting

    shap_values = simple_xgb(train_instances, None, target_first_encoded, None, top_setting, explainer=True)
    feature_importances = np.mean(shap_values, axis=0)
    print(feature_importances.shape, len(train_instances.columns))
    assert len(feature_importances) == len(train_instances.columns)
    cnames = train_instances_real.columns
    out_result = {"importances": feature_importances, "names": cnames, "dataset": dataset, "top_acc": top_acc,
                  "top_param": top_setting, "method": "xgb"}
    return out_result


def mutual_info_rankings(dataset):
    train_instances_real, target_first = load_and_process_generic(dataset)
    cnames = train_instances_real.columns
    feature_importances = mutual_info_classif(train_instances_real, target_first)
    out_result = {"importances": feature_importances, "names": cnames, "dataset": dataset, "method": "MI"}
    return out_result


def attention_rankings(dataset):
    train_instances_real, target_first = load_and_process_generic(dataset)
    enc0 = preprocessing.LabelEncoder()
    enc1 = preprocessing.OneHotEncoder()
    target_first_encoded = enc0.fit_transform(target_first)
    skf = StratifiedKFold(n_splits=3)

    train_instances = train_instances_real    
    learning_rates = [0.001,0.01,0.00001]#[0.001,0.1,0.0001, 0.00001]
    batch_size = [256]
    dropouts = [0.2,0,0.5]#[0.01,0.05,0.1,0.2,0.3,0.5,0.8]
    num_epoch = [5,20]#[5,10,15,20,100, 200]
    hidden_size = [128,64]#[128,256,512,1024]
    grid_nn = []
    
    for l in learning_rates:
        for b in batch_size:
            for e in num_epoch:
                for h in hidden_size:
                    for d in dropouts:
                        grid_nn.append((h,e,b,l,d))

    top_acc = 0
    top_setting = None
    ## do a split on the train set
    print("Starting testing")
    for parameter_setting in tqdm.tqdm(grid_nn):
        print("Testing {} setting.".format(parameter_setting))
        overall_acc = []        
        for train_index, test_index in skf.split(train_instances, target_first_encoded):
            train_features = train_instances.iloc[train_index, :]
            test_features = train_instances.iloc[test_index, :]
            assert set(train_features.columns) == set(test_features.columns)
            train_tars = target_first_encoded[train_index]
            test_tars = target_first_encoded[test_index]
            acc = simple_ffNN(train_features, test_features, train_tars, test_tars, parameter_setting)

            overall_acc.append(acc)
        acc = np.mean(overall_acc)
        if top_acc < acc:
            print("Improved accuracy to {}%".format(acc))
            top_acc = acc
            top_setting = parameter_setting

    ## get attention
    attention = "attention"
    attentions, clean_attentions, self_attention = simple_ffNN(train_instances, None, target_first_encoded, None, top_setting)
    all_at = torch.cat(attentions)
    all_at = torch.sum(all_at, 0).cpu()
    feature_importances = all_at.numpy()
    cnames = train_instances_real.columns
    out_result1 = {"importances": feature_importances, "names": cnames, "dataset": dataset, "top_acc": top_acc,
                  "top_param": top_setting, "method": "attention"}

    all_at = torch.cat(clean_attentions)
    all_at = torch.sum(all_at,0).cpu()
    feature_importances = all_at.numpy()

    ## clean attention
    out_result2 = {"importances":feature_importances,"names":cnames,"dataset":dataset,"top_acc":top_acc,"top_param":top_setting,"method":attention+"Clean"}

    ## whole layer
    out_result3 = {"importances":self_attention,"names":cnames,"dataset":dataset,"top_acc":top_acc,"top_param":top_setting,"method":attention+"Layer"}
    
    return out_result1, out_result2, out_result3


def genie_and_relief_ranking(arff_file, ranking_type, fimp_file):
    def convert_fimp_to_pickle(fimp, pickle_file):
        data_set = os.path.basename(fimp)
        data_set = data_set[:data_set.find('_')]
        feature_importance = []
        column_names = []
        top_accuracy = None
        method = "forest" if "Genie3" in fimp else "relief"
        top_configuration = {"relief": ["15", "all"], "forest": ["RForest", "Genie3", "100"]}
        with open(fimp) as pickle_f:
            for l in pickle_f:
                if l.startswith("-------"):
                    break
            for l in pickle_f:
                line = l.strip().split('\t')
                column_names.append(line[1])
                feature_importance.append(eval(line[3])[0])
        result = {"importances": feature_importance, "names": column_names,
                  "dataset": data_set,
                  "top_acc": top_accuracy,
                  "top_param": top_configuration[method], "method": method}
        with open(pickle_file, 'wb') as pickle_f:
            pickle.dump(result, pickle_f, protocol=pickle.HIGHEST_PROTOCOL)
        return result

    s_pattern = """[General]
ResourceInfoLoaded = Yes
Verbose = 0

[Data]
File = {0}

[Attributes]
{1}
{2}

[Ensemble]
Iterations = 100
NumberOfThreads = 1
EnsembleMethod = RForest
FeatureRanking = Genie3
SelectRandomSubspaces = SQRT
Optimize = No
PrintAllModels = No
PrintAllModelFiles = No
PrintAllModelInfo = No
PrintPaths = No
OOBestimate = No
SortRankingByRelevance = No

[Relief]
Neighbours = 15

[Output]
WritePredictions = [Test]
    """
    assert ranking_type in ["relief", "forest"]
    arff_absolute = os.path.abspath(arff_file)
    data_name = os.path.basename(arff_file[:arff_file.rfind("_train")])
    temp_dir = ""
    for i in range(1, 10**3):
        temp_dir = os.path.abspath("temp{}".format(i))
        if not os.path.exists(temp_dir):
            os.makedirs(temp_dir)
            break
    if not temp_dir:
        raise ValueError("...")
    # experiment data
    target_str, key_str = find_target_key_n_features(arff_absolute)
    s_file = os.path.join(temp_dir, "experiment.s")
    with open(s_file, "w", newline="") as f:
        print(s_pattern.format(arff_absolute, target_str, key_str), file=f)
    # run the experiment
    clus = os.path.abspath("../experiments_relief_genie/clus.jar")
    subprocess.call("java -Xmx10G -jar {} -{} {}".format(clus, ranking_type, s_file),
                    shell=True, stderr=stderr, stdout=stdout)
    # copy the results
    fold = int(re.search("fold([0-9]+)", arff_absolute).group(1))
    results_file = "../rankings_by_folds/{}_{}_{}.pickle".format(ranking_type, fold, data_name)
    r = convert_fimp_to_pickle(os.path.join(temp_dir, fimp_file), results_file)
    shutil.rmtree(temp_dir)
    return r


def genie_ranking(arff_file):
    return genie_and_relief_ranking(arff_file, "forest", "experimentTrees100Genie3.fimp")


def relief_ranking(arff_file):
    return genie_and_relief_ranking(arff_file, "relief", "experiment.fimp")


if __name__ == "__main__":
    import argparse
    import glob

    parser = argparse.ArgumentParser()
    parser.add_argument("--dataset", default="../data")
    parser.add_argument("--method", default="attention")
    args = parser.parse_args()
    
    for dataset in glob.glob(args.dataset + "/*"):
        dataset_name = dataset.split("/")[-1].replace(".csv", "") + ".pickle"
        raise ValueError("To se bo sesul na Windows:)")
        train_instances_real, target_first = load_and_process_generic(dataset)
        if train_instances_real is None:
            continue
        enc0 = preprocessing.LabelEncoder()
        enc1 = preprocessing.OneHotEncoder()
        target_first_encoded = enc0.fit_transform(target_first)
        skf = StratifiedKFold(n_splits=10)

        train_instances = train_instances_real
        overall_acc = []

        learning_rates = [0.001, 0.1, 0.0001]
        batch_size = [10, 20, 100]
        num_epoch = [5, 10, 15, 20, 100]
        hidden_size = [128, 256, 512, 1024]
        grid_nn = []
        for l in learning_rates:
            for b in batch_size:
                for e in num_epoch:
                    for h in hidden_size:
                        grid_nn.append((h, e, b, l))

        xgb_grid = []
        # grid_xgb = {"learning_rate"    : [0.05] ,
        #             "max_depth"        : [ 3],
        #             "min_child_weight" : [ 1],
        #             "gamma"            : [ 0.0],
        #             "estimators"            : [10],
        #             "colsample_bytree" : [ 0.3 ] }

        grid_xgb = {"learning_rate": [0.05, 0.30,0.001],
                    "max_depth": [3, 10, 15],
                    "min_child_weight": [1, 5,10],
                    "gamma": [0.0, 0.05,0.1, 0.2, 0.3, 0.4],
                    "estimators": [100,200,300],
                    "colsample_bytree": [0.3, 0.7]}

        for e in grid_xgb['estimators']:
            for d in grid_xgb['max_depth']:
                for lr in grid_xgb['learning_rate']:
                    for mcw in grid_xgb['min_child_weight']:
                        for g in grid_xgb['gamma']:
                            for cs in grid_xgb['colsample_bytree']:
                                xgb_grid.append([e, d, lr, mcw, g, cs])

        grid_search = {"attention": grid_nn, "xgb": xgb_grid}
        top_acc = 0
        top_setting = None
        for parameter_setting in tqdm.tqdm(grid_search[args.method]):
            print("Testing {} setting.".format(parameter_setting))
            for train_index, test_index in skf.split(train_instances, target_first_encoded):
                train_features = train_instances.iloc[train_index, :]
                test_features = train_instances.iloc[test_index, :]
                assert set(train_features.columns) == set(test_features.columns)
                train_tars = target_first_encoded[train_index]
                test_tars = target_first_encoded[test_index]

                if args.method == "attention":
                    acc = simple_ffNN(train_features, test_features, train_tars, test_tars, parameter_setting)
                if args.method == "xgb":
                    acc = simple_xgb(train_features, test_features, train_tars, test_tars, parameter_setting)

                overall_acc.append(acc)
            acc = np.mean(overall_acc)
            if top_acc < acc:
                print("Improved accuracy to {}%".format(acc))
                top_acc = acc
                top_setting = parameter_setting

        # compute overall attention
        if args.method == "attention":
            attentions = simple_ffNN(train_instances, None, target_first_encoded, None, top_setting)
            all_at = torch.cat(attentions)
            all_at = torch.sum(all_at, 0).cpu()
            feature_importances = all_at.numpy()

        if args.method == "xgb":
            shap_values = simple_xgb(train_instances, None, target_first_encoded, None, parameter_setting,
                                     explainer=True)
            feature_importances = np.mean(shap_values, axis=0)
            assert len(feature_importances) == len(train_instances.columns)
        else:
            print("No method")

        cnames = train_instances_real.columns
        out_result = {"importances": feature_importances, "names": cnames, "dataset": dataset, "top_acc": top_acc,
                      "top_param": top_setting, "method": args.method}
        with open('../results/' + args.method + "_" + dataset_name, 'wb') as handle:
            pickle.dump(out_result, handle, protocol=pickle.HIGHEST_PROTOCOL)
