import torch
import tqdm
import torch.nn as nn
from sklearn.preprocessing import OneHotEncoder,LabelEncoder
from torch.utils.data import DataLoader, Dataset
import logging
logging.basicConfig(format='%(asctime)s - %(message)s', datefmt='%d-%b-%y %H:%M:%S')
logging.getLogger().setLevel(logging.INFO)
import numpy as np

class ffNN(nn.Module):
    def __init__(self, input_size,num_classes,hidden_size):
        super(ffNN, self).__init__()
        self.fc1 = nn.Linear(input_size, input_size)
        self.softmax = nn.Softmax(dim=1)
        self.relu = nn.ReLU()
        self.dropout = nn.Dropout(0.2)
        self.sigma = nn.Sigmoid()
        self.fc2 = nn.Linear(input_size, hidden_size)
        self.fc3 = nn.Linear(hidden_size, num_classes)
    def forward(self, x):
        out = self.fc1(x)
        out = self.relu(out)
        out = self.fc2(out)
        out = self.dropout(out)
        out = self.fc3(out)
        out = self.sigma(out) 
        return out

class DatasetLoader_e2e(Dataset):
    
    def __init__(self, features, targets, transform=None):
        self.features = features        
        self.targets = targets
        
    def __len__(self):
        return self.features.shape[0]
    
    def __getitem__(self, index):
        instance = torch.from_numpy(self.features[index, :])
        if self.targets is not None:
            target = torch.as_tensor(self.targets[index].todense())
        else:
            target = None
        return (instance,target)

def to_one_hot(lbx):
    enc = OneHotEncoder(handle_unknown='ignore')
    trained_encoder = LabelEncoder().fit(lbx.values)
    trained_encoder_code = trained_encoder.transform(lbx.values)
    encoded = np.array(trained_encoder_code)
    return (enc.fit_transform(encoded.reshape(-1,1)),trained_encoder)
        
class e2e_DNN:

    def __init__(self,batch_size=1,num_epochs=25,learning_rate=0.0001,stopping_crit=5,hidden=64):
        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        self.loss = nn.BCELoss()
        self.stopping_crit = stopping_crit
        self.num_epochs = num_epochs
        self.learning_rate = learning_rate
        self.hidden = hidden
        
    def fit(self,features,labels):

        labels,label_encoder = to_one_hot(labels)
        self.label_encoder = label_encoder
        train_dataset = DatasetLoader_e2e(features.values, labels)
        total_step = len(train_dataset)
        stopping_iteration = 0
        loss = 1
        current_loss = 0
        self.model = ffNN(features.shape[1],labels.shape[1],self.hidden).to(self.device)
        self.optimizer = torch.optim.Adam(self.model.parameters(), lr=self.learning_rate)
        self.num_params = sum(p.numel() for p in self.model.parameters())
        for epoch in range(self.num_epochs):
            if current_loss != loss:
                current_loss = loss
            else:
                stopping_iteration+=1
            if stopping_iteration > self.stopping_crit:
                break
            losses_per_batch = []
            for i, (features,labels) in enumerate(train_dataset):
                features = features.float().to(self.device)
                labels = labels.float().to(self.device)
                outputs = self.model.forward(features)
                loss = self.loss(outputs, labels)
                self.optimizer.zero_grad()
                loss.backward()
                self.optimizer.step()
                losses_per_batch.append(float(loss))
            mean_loss = np.mean(losses_per_batch)
          #  logging.info("Current loss {}, epoch {}".format(mean_loss,epoch))
            
    def predict(self,features):
        test_dataset = DatasetLoader_e2e(features.values, None)
        predictions = []
        with torch.no_grad():
            for features,_ in test_dataset:
                features = features.float().to(self.device)
                representation = self.model.forward(features)
                values,indices = torch.max(representation,0)
                rep_ind = indices.cpu().numpy()
                indices = self.label_encoder.inverse_transform(rep_ind)
                predictions.append(indices)
        return predictions
